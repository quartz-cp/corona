package app.quartzcp.corona;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Arrays;

public final class Corona extends JavaPlugin {
    @Override
    public void onEnable() {
        getLogger().info("Corona plugin enabled");

        ServerHandler handler = new ServerHandler();

        try {
            double[] serverTPS = handler.getServerTPS();
            System.out.println(Arrays.toString(serverTPS));

            String motd = handler.getMotd();
            System.out.println(motd);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDisable() {
        getLogger().info("Corona plugin disabled");
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if ("corona".equals(command.getName())) {
            sender.sendMessage("Hello, world!");
            return true;
        }

        return false;
    }
}
