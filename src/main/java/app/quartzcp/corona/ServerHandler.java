package app.quartzcp.corona;

import org.bukkit.Bukkit;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class ServerHandler {
    private Object nmsServer;
    private Object properties;

    private Field recentTps;
    private Field motd;

    ServerHandler() {
        try {
            Class<?> craftServerClass = getCraftbukkitClass("CraftServer");
            Method getServer = craftServerClass.getMethod("getServer");
            this.nmsServer = getServer.invoke(Bukkit.getServer());
            Class<?> nmsServerClass = nmsServer.getClass();
            this.recentTps = nmsServerClass.getField("recentTps");

            Object propertyManager = nmsServerClass.getField("propertyManager").get(nmsServer);
            Class<?> propertyManagerClass = propertyManager.getClass();
            this.properties = propertyManagerClass.getMethod("getProperties").invoke(propertyManager);
            Class<?> propertiesClass = properties.getClass();
            this.motd = propertiesClass.getDeclaredField("motd");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public double[] getServerTPS() throws IllegalAccessException {
        if (this.recentTps == null) {
            return null;
        }

        return (double[]) this.recentTps.get(nmsServer);
    }

    public String getMotd() throws IllegalAccessException {
        if (this.motd == null) {
            return null;
        }

        return (String) this.motd.get(properties);
    }

    private static Class<?> getCraftbukkitClass(String craftbukkitClassString) throws ClassNotFoundException {
        String version = getServerVersion();
        String name = "org.bukkit.craftbukkit." + version + "." + craftbukkitClassString;
        return Class.forName(name);
    }

    private static Class<?> getNMSClass(String nmsClassString) throws ClassNotFoundException {
        String version = getServerVersion();
        String name = "net.minecraft.server." + version + "." + nmsClassString;
        return Class.forName(name);
    }

    private static String getServerVersion() {
        return Bukkit.getServer().getClass().getPackage().getName().substring(23);
    }
}
